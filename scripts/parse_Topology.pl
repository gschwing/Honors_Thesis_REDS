#!/~/bin/perl

my $filename = 'topol.top';

open( my $fh, '<:encoding(UTF-8)', $filename)
	or die "Could not open file '$filename'!";

my $protein_molecules;
my $water_molecules;


my @lines;
while ( <$fh> ) { 
    next unless m/\[ atoms \]/;
    # in an list context, this will slurp the rest of the file.
    @lines = <$fh>;
}

#print "@lines\n";
my $counter = 0;

####################
#I marked two lines, the one near num_p_atoms and num_waters
#Then I adjust index to get the exact line, did it this way
#so I can search for unique tags .i.e "\[ bonds \]"
#and just grab the data from the nearby line
#
#This is ugly but this is a prototype
#
#Anyway, since I will be using this in a closed enviroment where the experiment is scripted
#I can set it up to be compatible with the other scripts to ensure functionality
#



#my $spot_for_num_atoms;
my $spot_1;


#my $spot_for_num_waters;
my $spot_2;

foreach $str (@lines){
	$counter++;	
	if ($str =~ m/\[ bonds \]/){
		$spot_1 = $counter;
	}
	if ($str =~ m/\[ molecules \]/){
		$spot_2 = $counter;
	}
}

#print "$lines[$spot_1-3]\n\n";
#print "$lines[$spot_2+2]\n\n";

$protein_atoms = $lines[$spot_1-3];
$water_molecules = $lines[$spot_2+2];

$water_molecules =~ s/\D//g;
( $protein_atoms ) = $protein_atoms =~ /(\d+)/;
print "$protein_atoms $water_molecules";
#print "\nThere are $protein_atoms protein atoms and $water_molecules water molecules.\n\n";

#!/usr/bin/perl

use IPC::System::Simple qw(system capture);

######### Parse Topology.topol for Num_waters and num_proteins ########

my $results0 = capture($^X, "parse_Topology.pl", @ARGS);

my @number_of_molecules = split / /, $results0; 

print "@number_of_molecules\n";
########## Generate replica temperatures using online resource ################
########## number_of_molecules = (# of protein molecules, # of water molecules)	####

my $results1 = capture($^X, "temp_gen.pl", @number_of_molecules);

print "Results 1: $results1\n";

my @temps = split / /, $results1;

print "This is the temps I get back: @temps\n";

#print "@temps\n";

###########	http://www.perlmonks.org/?node_id=45928
######### Get files in current directory ##################

opendir my $dir, "." or die "Cannot open directory: $!";

my @files = readdir $dir;

closedir $dir;

###########

######## Get the name of the file template ###########

my $file_template;

for (my $i = 0; $i < scalar @files; $i++) {
#	print "$files[$i] ";
	if (($files[$i] =~ /ZZZ.mdp/)){
		$file_template = $files[$i];	
	}
}


###### The ZZZ file template #########
print "The file template: $file_template\n";


##### Add name of template file to beginning of replica_template array

unshift @temps, $file_template;


#### Call generator with args consisting of the template and the values of 
#### Temperatures to use as replacement vals for Z
print "Calling generate_ZZZ with arguments @temps\n";

#pop @temps;
#pop @temps;

system('./generate_ZZZ.pl', @temps);

system('./organize.pl');

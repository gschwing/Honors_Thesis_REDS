#!/usr/bin/perl

use WWW::Mechanize;
use strict;
use warnings;
my $mech = WWW::Mechanize->new;

my $Np = shift @ARGV;
#print "Num prot: $Np\n";
my $Nw = shift @ARGV;
#print "Num water: $Nw\n";


$mech->get('http://folding.bmc.uu.se/remd/');

$mech->submit_form(
  fields => {
  
	'Pdes'=>'0.2',
	'Tol' => '1e-4',
	'Tlow' => '300',
	'Thigh' => '325',
	'Nw' => $Nw,
	'WC' => '0',
	'Np' => $Np,
	'PC' => '0',
	'Hff' => '0',
	'Vs' => '0',
	'Alg' => '0',

	},
);

my $output =  $mech->content(format => 'text');
#my $output =  $mech->content;

#print $output;

my @words = split / /, $output;
my @temps;

for(my $i = 0; $i < scalar @words; $i++){
	if ($words[$i] eq 'Enjoy.'){
		until ($words[$i++] eq 'If'){
			$words[$i] =~ s/,//ig;
			push @temps, $words[$i];
		}
	}
}
pop @temps;

my $str = pop @temps;

$str =~ s/\s+$//;

push @temps, $str;

print "@temps";	 

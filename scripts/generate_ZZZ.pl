#!/usr/bin/perl

###############################################################
################	USAGE	###############################
#	perl generate_ZZZ.pl -arg1 -arg2...N
#	arg1 	: a file containing of this format *_ZZZ.*	#######
#	arg2...N: the temperatures to replace ZZZ with
#
###############################################################	

#use strict;
#use warnings;
#use diagnostics;

print "Generate_ZZZ called\n";

use File::Copy "cp";

print "The arguments before 1 shift: @ARGV\n";

my $file_template = shift(@ARGV);

print "The arguments after 1 shift: @ARGV\n";


#Tokenize arg0 by period separator
my @fields = split /_ZZZ./, $file_template;

#save local variables from temp name
my $prefix = $fields[0] . '_';
my $suffix = '.' . $fields[1];

#populate array = replicas redirected by temp_gen.pl
#################################################################
#################################################################

@replica_temps = @ARGV;

print "The replica temperatures: @replica_temps\n";

my $counter =0;

foreach (@replica_temps)
{
	# generate new name
	my $new_file = $prefix.$counter.$suffix;	

	#print $new_file;	
	cp($file_template, $new_file) or die ("copy $new_file failed");
	
	$counter++;
	# Replace ZZZ the temp
	`perl -pi -e 's/ZZZ/$_/' $new_file`;
	`perl -pi -e 's/ZZZ/$_/' $new_file`
		
}
